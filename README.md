Port Pool
===================
This module can create a port pool and return the next free port on request.
Ports are not reserved at system level. Port will be checked automatically.
On every request the full pool is checked again (this means ports will be recycled when they are free again).

Creating a pool
-------------
Creating a pool happens in the constructor, like so:

    var portPool = new PortPool(5000, 6000);

Requesting a free port
-------------
To request a free port simply call the "getNext" function:

    portPool.getNext(function(port) {
		// port is the port number
		// port is null when no free port is found
	});