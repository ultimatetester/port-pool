const net = require('net');

const PortPool = function (start, end) {
    this.start = start;
    this.end = end;
};

PortPool.prototype.getNext = function (next) {
    const self = this;

    function checkLoop(port) {
        if (port > self.end) {
            next(null);
            return;
        }

        self.checkPortStatus(port, function (free) {
            if (free === false) {
                checkLoop(port + 1);
                return;
            }

            next(port);
        });
    }

    checkLoop(self.start);
};

PortPool.prototype.checkPortStatus = function (port, next) {
    const server = net.createServer();

    server.on('error', function () {
        next(false);
    });

    server.on('listening', function () {
        server.on('close', function () {
            next(true);
        });

        server.close();
    });

    server.listen({host: 'localhost', port: port, exclusive: true});
};

module.exports = PortPool;