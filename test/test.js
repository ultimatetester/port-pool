var assert = require('chai').assert;
var path = require('path');
var PortPool = require('../index');

describe('Port Selection', function () {
    var portPool = null;

    before(function (done) {
        portPool = new PortPool(50000, 60000);
        done();
    });

    it('should return first available port', function (done) {
        portPool.getNext(function (port) {
            assert.equal(port, 50000, 'First port should match 50000');
            done();
        });
    });
});